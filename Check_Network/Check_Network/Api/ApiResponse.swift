//
//  ApiResponse.swift
//  Check_Network
//
//  Created by Luyện Đào on 23/08/2022.
//

import Foundation
import Moya
import SwiftyJSON

public class ApiResponse {
    
    let error: ErrorClient?
    let data: JSON?
    
    init(response: Response) {
        if response.isUnauthenticated {
            let json = JSON(response.data)
            self.error = ErrorClient(
                errorCode: json["errorCode"].stringValue,
                message: json["messages"].string ?? "",
                status: ServiceHelper.errorCodeUnauthenticated)
            self.data = nil
//            Session.shared.logout()
        } else if response.isUnauthorized {
            let json = JSON(response.data)
            self.error = ErrorClient(
                            errorCode: json["errorCode"].stringValue,
                            message: json["messages"].string ?? "",
                            status: ServiceHelper.errorCodeUnauthorized)
            self.data = nil
        } else if response.isGatewayTimeOut {
            self.error = ErrorClient(
            errorCode: "error",
            message: "network.error.request.timed.out".localized(),
            status: ServiceHelper.errorCodeGatewayTimeOut)
            self.data = nil
        } else if response.isSuccess {
          
            let json = JSON(response.data)
            if (json["success"].exists() &&  json["success"].bool == true) {
                self.error = nil
            } else {
                self.error = ErrorClient(
                errorCode: json["errorCode"].stringValue,
                message: json["messages"].string ?? "Something went wrong!",
                status: response.statusCode)
            }
            self.data = json
            self.parsingData(json: json)
        } else {
            let json = JSON(response.data)
            self.error = ErrorClient(
            errorCode: json["errorCode"].stringValue,
            message: json["messages"].string ?? "Something went wrong!",
            status: response.statusCode)
            self.data = nil
        }
    }
    
    init(error: Error) {
        let nsError = error as NSError
        self.error = ErrorClient(
        errorCode: nsError.domain,
        message: nsError.localizedDescription,
        status: nsError.code)
        self.data = nil
    }
    
    // Override to parsing data
    func parsingData(json: JSON) {
        
    }
    
    
}

struct ErrorClient: Error {
    var errorCode: String
    var message: String
    var status: Int
}

extension Moya.Response {
    
    var isSuccess: Bool {
        return statusCode / 100 == 2
    }
    
    var isUnauthenticated: Bool {
        return statusCode == ServiceHelper.errorCodeUnauthenticated
    }
    
    var isUnauthorized: Bool {
        return statusCode == ServiceHelper.errorCodeUnauthorized
    }
    
    var isGatewayTimeOut: Bool {
        return statusCode == ServiceHelper.errorCodeGatewayTimeOut
    }
}

struct ServiceHelper {
    static let errorServiceDomain = "com.vmodev.error.service"
    static let errorMoyaDomain = "Moya.MoyaError"
    static let errorCodeUnauthenticated = 401
    static let errorCodeUnauthorized = 403
    static let errorCodeGatewayTimeOut = 504
}

extension  String  {
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}


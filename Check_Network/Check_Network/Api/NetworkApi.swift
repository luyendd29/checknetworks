//
//  NetworkApi.swift
//  Check_Network
//
//  Created by Luyện Đào on 23/08/2022.
//

import Foundation
import Moya

public enum NetworkAPI {
    case startUploadDocument(params: Dictionary<String, Any>)
    case getList(params: Dictionary<String, Any>)
}

extension NetworkAPI: TargetType {
    
    public var baseURL: URL {
        return URL(string: "https://themealdb.p.rapidapi.com")!
    }
    
    public var path: String {
        switch self {
        case .startUploadDocument:
            return "/documents/startUploadDocument"
        case .getList:
            return "/list.php"
        }
    }
    
    public var method: Moya.Method {
        switch self {
       
        case .startUploadDocument:
            return .post
        case .getList:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .startUploadDocument(let addPrams):
            return .requestCompositeParameters(bodyParameters: addPrams, bodyEncoding: JSONEncoding.default, urlParameters: [:])
        case .getList(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .getList:
            return [
                "X-RapidAPI-Key" : "3eaa55e25cmshdb95e461cca8827p16799fjsn4425f9e011f3",
                "X-RapidAPI-Host" : "themealdb.p.rapidapi.com"
            ]
        default:
            return [:]
        }
        
    }
}


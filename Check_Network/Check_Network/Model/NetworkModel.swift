//
//  NetworkModel.swift
//  Check_Network
//
//  Created by Luyện Đào on 23/08/2022.
//

import Foundation
import SwiftyJSON

struct ListMealsModel: Codable {
    var meals: [ListMeal]?
    
    struct ListMeal: Codable {
        var strArea: String?
    }
}

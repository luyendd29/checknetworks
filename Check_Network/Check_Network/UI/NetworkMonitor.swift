//
//  NetworkMonitor.swift
//  Check_Network
//
//  Created by Luyện Đào on 24/08/2022.
//

import Foundation


class NetworkMonitor {
    static func addListener(about: EventCase, didReceive:@escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: about.rawValue),
            object: nil,
            queue: OperationQueue.main,
            using: didReceive)
    }
}

//
//  ShowPopupViewController.swift
//  Check_Network
//
//  Created by Luyện Đào on 24/08/2022.
//

import UIKit

class ShowPopupViewController: UIViewController {

    var closurePass: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func tapOK(_ sender: Any) {
        dismiss(animated: true, completion: closurePass)
    }

}

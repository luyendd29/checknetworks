//
//  HomeViewController.swift
//  Check_Network
//
//  Created by Luyện Đào on 22/08/2022.
//

import UIKit
import Moya

final class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var listMeal = ListMealsModel()
    var params = Dictionary<String, Any>()
    let provider = MoyaProvider<NetworkAPI>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkMonitor.addListener(about: .networkState) { notification in
            if let isInternetOn = notification.userInfo?["networkConnect"] as? Bool {
                if isInternetOn {
                    self.dismiss(animated: true)
                    self.getListMeal()
                }
            }
            
            if let isInternetOff = notification.userInfo?["networkDisConnect"] as? Bool {
                if isInternetOff {
                    self.showPopup()
                }
            }
        }
        self.getListMeal()
        registerCell()
    }
    
    func showPopup() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ShowPopupViewController") as? ShowPopupViewController
        vc?.modalPresentationStyle = .overFullScreen
        vc?.modalTransitionStyle = .crossDissolve
        vc?.closurePass = { [weak self] in
            self?.listMeal.meals?.removeAll()
            self?.tableView.reloadData()
        }
        
        present(vc!, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    private func registerCell() {
        tableView.register(UINib(nibName: "ListMealTableViewCell", bundle: nil), forCellReuseIdentifier: "ListMealTableViewCell")
    }
    
    private func getListMeal() {
        params.removeAll()
        params["a"] = "list3"
        getListMeal(params: params)
    }
    
    private func getListMeal(params: Dictionary<String, Any>) {
        provider.request(.getList(params: params)) {[weak self] result in
            switch result {
            case let .success(moyaResponse) :
                do {
                    let results = try JSONDecoder().decode(ListMealsModel.self, from: moyaResponse.data)
                    self?.listMeal = results
                    self?.tableView.reloadData()
                } catch let error {
                    debugPrint(error)
                }
                
            case let .failure(error):
                debugPrint(error)
                break
            }
        }
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMeal.meals?.count ?? 0
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ListMealTableViewCell", for: indexPath) as? ListMealTableViewCell
        let list = listMeal.meals
        cell?.nameLabel.text = list?[indexPath.row].strArea
        return cell!
    }
    
}

extension UIApplication {
    
    func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController {
        
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        
        return base!
    }
}
